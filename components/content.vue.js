const Content = {
  template: `
    <div>
      <div class="card-content">
        <div class="btn-back" @click="back">&#8592; Back to Mile</div>
        <div class="content">
          <div class="section-left">
            <div class="p-5">
              <div class="logo">
                <img src="assets/logo.png" class="img-size" alt="">
              </div>

              <div class="desc">
                Your one stop platform to manage all of your field service management
              </div>

              <div>
                <input v-model="form.organization" class="input-form" placeholder="Enter your organization's name">
                <div v-if="error" class="error">{{ message }}</div>
                <button class="btn btn-primary" @click="login">Login</button>
                <div class="desc-register">
                  Not registered yet?
                  <a href="https://taskdev.mile.app/request-demo" target="_blank" class="contact-us">Contact us </a> for more info
                </div>
              </div>
            </div>
            <!-- <div class="footer">
              © Copyright 2019 PT. Paket Informasi Digital. All Rights Reserved
            </div> -->
          </div>
          <div class="section-right bg">
            <div class="about">
              <div class="title">About Mile</div>
              <small>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo reiciendis praesentium repellendus corrupti asperiores itaque cumque non! Veritatis voluptatem quibusdam quisquam, sit, consequuntur vero delectus deserunt, fugit earum est voluptatibus.</small>
            </div>
            <div class="about">
              <div class="title">Features</div>
              <ul>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. llo reiciendis praesentium repellendus corrupti asperiores</li>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. llo reiciendis praesentium repellendus corrupti asperiores</li>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. llo reiciendis praesentium repellendus corrupti asperiores</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>`,

    data() {
      return {
        form: {
          organization: '',
        },
        error: false,
        message: '',
      };
    },

    methods: {
      login() {
        this.error = true;

        if (!this.form.organization) {
          this.showMsg('The organization field is required');
          return;
        }

        this.showMsg('oops :( data is not available');
      },

      showMsg(msg) {
        this.message = msg;
      },

      back() {
        window.location = 'https://taskdev.mile.app/login';
      }
    },
};