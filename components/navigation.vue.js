const Navigation = {
  template: `
    <div class="app-header">
      <div>
        <span class="mr-1">Call Us Now: +62 812-1133-5608</span>
        <span @click="toggle" class="language">
          {{ language }}
          <i :class="['fas', isShow ? 'fa-angle-up' : 'fa-angle-down']"></i>
        </span>
        <div :class="[{'is-show': isShow}, 'dropdown-menu']">
          <div class="dropdown-item" @click="handleSelected('Indonesia')">
            Indonesia
          </div>

          <div class="dropdown-item" @click="handleSelected('English')">
            English
          </div>
        </div>
      </div>
    </div>
  `,

  data() {
    return {
      isShow: false,
      language: 'English'
    };
  },

  methods: {
    toggle() {
      this.isShow = !this.isShow
    },

    handleSelected(lang) {
      this.language = lang;
      this.toggle();
    }
  },
};